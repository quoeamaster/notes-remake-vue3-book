module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      animation: {
        'bounce-once': 'bounce-once-frame 1s',
        'bounce-once-out': 'bounce-once-out-frame 0.3s',
      },
      keyframes: {
        'bounce-once-frame': {
          '0%': { transform: 'translateY(50%)' },
          '50%': { transform: 'translateY(-8%)' },
          '70%': { transform: 'translateY(8%)' },
          '80%': { transform: 'translateY(-5%)' },
          '90%': { transform: 'translateY(3%)' },
          '100%': { transform: 'translateY(0)' }
        },
        'bounce-once-out-frame': {
          '0%': { transform: 'translateY(0%)' },
          '100%': { transform: 'translateY(100%)' }
        },
      }
    },
  },
  plugins: [],
}
