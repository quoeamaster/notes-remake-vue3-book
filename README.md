
## notes remake - vue3 book example

As an example for the book [build real world Vue3 apps with Vite, TailwindCSS and Elasticsearch](https://www.amazon.com/); we are going to build a [notes-app](https://kahoot.com/what-is-kahoot) with [Vue3.js](https://vuejs.org/) and [Vite](https://vitejs.dev/) and [BootstrapCSS](https://getbootstrap.com/docs/3.4/css/). 

The notes-app is a full-fledged and self-maintained application for taking notes in 3 formats:
 - plain text notes,
 - list based notes AND
 - photo notes

#### features

   - able to list out all available notes (of different types)
   - provides a search bar for filtering out notes matching the title's value with the given keywords
   - able to add / edit notes of a particular type (plain-text, list, photo)

#### page flow

   - listing page -> listing all available notes if available
   - add new notes through the bottom menubar
   - for existing notes, click to enter "edit" mode
   - all pages provide an "info" button at the top-right, the corresponding "activity-view" would be shown based which page it is currently referencing
