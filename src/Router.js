// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// notes remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { createRouter, createWebHistory } from 'vue-router'
// component imports
import Home from './components/Home.vue';
import Note from './components/Note.vue';
import RouteNotExist from './components/RouteNotExist.vue';
import NotePlainText from './components/NotePlainText.vue';
import NoteList from './components/NoteList.vue';
import NotePhoto from './components/NotePhoto.vue';

const routes = [
   {
      path: "/",        // URL path to locate this route / component
      name: "Home",     // the name / ID for this route
      component: Home,  // actually vue component under this route
   },
   {
      path: "/note/:action",
      name: "Note",
      component: Note,
      props: true,      // :action is mapped to a prop named "action" in the Note component
      children: [
         {
            name: "notePlain",
            path: "",
            component: NotePlainText,  // the "plain text note"
         },
         {
            name: "noteList",
            path: "list",
            component: NoteList,  // the "plain text note"
         },
         {
            name: "notePhoto",
            path: "image",
            component: NotePhoto,  // the "plain text note"
         },
      ],
   },
   
   // the catch-all route
   {
      path: '/:pathMatch(.*)', 
      component: RouteNotExist,
   },
]

const router = createRouter({
   history: createWebHistory(),
   routes,
})

export default router

// router setup under vue3 : https://www.vuemastery.com/blog/vue-router-a-tutorial-for-vue-3%