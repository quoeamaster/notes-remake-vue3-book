// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// notes remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// setup vuex
import { createStore } from 'vuex'

// setup store
const store = createStore({
   state() {
      return {
         /**
          *  notes - the array of notes object
          * 
          *  suggested architecture of a "note" object
          * {
          *    "title": "name-of-the-note",
          *    "tags": [ "default" | "any-valid-tag" ]
          *    "type": "plain | photo | checklist | table | freeform",
          * 
          *    # [deprecated] "folder": "all | any-valid-folder-name",
          * 
          *    # the contents contain rows of data to be displayed, the components within differs based on the "type" of note created.
          *    # e.g. "freeform" notes contains not just the "src" but also a x,y tuple.
          *    # e.g. "photo" notes contains also the photo "url" and "src" for the text describing the photo.
          *    "contents": [
          *       { "src": "hello <b>Jollie</b>, welcome to the team~" }
          *    ]
          * }
          */
         /* --- plain-text ---
          * let _note = {
          *  'title': this.noteName,
          *  'type': 'plain',
          *  'tags': this.tags,
          *  'contents': this.$refs.noteArea.innerHTML, // String in this case...
          * };
          */
         //notes: [],
         notes: [
            // [debug] remove after testing / development
            // sample note(s)...
            {
               title: 'database study NOTES {2022-Feb}',
               type: 'plain',
               tags: [ 'default', 'database', 'study notes', '2002' ],
               contents: `<div>database targets:</div> <div></div> <div></div> <div>- clickhouse</div> <div>  - elasticsearch</div> <div> - mySQL</div> <div></div> <div>Next...</div> </div></div>`
            },
            { title: 'sample vegetable list',type: 'list',tags: [ 'default', 'veg list', 'family', '2002' ],contents: [{'checked': false, 'message': 'broccoli',},{'checked': true, 'message': '[sweet] potatoes x 2 bags',},{'checked': true, 'message': 'ginger x 2',},{'checked': false, 'message': 'spinach / spinach paste',},{'checked': true, 'message': 'parsley paste',}]},
            { title: 'elasticsearch tutorial',type: 'image',tags: [ 'default', 'elasticsearch', 'database', 'big data' ],contents: { message: '<div>tutorial</div><br/><br/><div>* for fun</div><div>   * big data SQL engines</div>', iSrc: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATcAAACiCAMAAAATIHpEAAAAw1BMVEX////+zQcAAAD9ygD32lv62V358LX52mX777n52Vr68LztISSrq6u7u7vY2dkYGBj44Xnr6+zHx8fNzc331klXV1fBwcFERET87rH39/f33GSkpKT55ZPtFhreW1qxsbHuyMflaWhxcXGMjIxkZGT/0gxra2uUlJR7e3vpAAEiIiKcnJzg4OCCgoIODg48PDwrKyvpRhpNTU346J779tf9+uY2Njb89thcXFzlfWPlOADvz8PgiojbUVDv09H04H/788loI8h2AAAGB0lEQVR4nO2af7/aNBSHsXe77ioUC0KtdeJKhY4WuKJOnM7t/b8qkzS/SlLgHu+n4Pg+f0A5bdr0IWlOAr0eAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgJvm128Uvz1L7FZ4uFN85Ym98sTudeyl57hb4eELieXthY4ZHy91zHj70nPcrQBvNOCNBrzRgDca8EYD3mjAGw14owFvNOCNBrzRgDca8EYD3mjAGw14owFvNOCNBrzRgDca8EYD3mjAGw14owFvNK7D2yAINqeOCYNNEP3H6zwfF/GW9PNVwInnoQgwb0G9a1a2uQnZMYf7xpN8EjYi8zxfPK0yNC7gLcoCi9WgZ3kr2EbiL+bzxsuNG5HJGS33OejeW1/oelwU2URsLXuWNx4Y+cud7W34hMqQ6dwbb1HBWgoIWdOrepa32CNHctve5uxON5X5HGX8VXuLssm0peTn7e3T/SvJ777LjQNvgzLjQjuft7cX+j+Y3/kuN3RuVABv3+uYz9uM3WfmiZt+Oh5bHpLpsijWZR2xvSXssKR3hrdxWRTzmTVCs4KR/UHtimZzfqmpdWgyYKFlyyjVrTf+2K88ce0ttRqela/0e01vLPsrVLkj3lJVPteu2Ie0Z32otUR7famJ2rlWkZmnwt16i1gttr5aaG99421qJXk8V7G8xarVHvWWbEXZjXgdGFV9fbDyFgXmSJk1RxuRLOU8VHhq3Km3qVTg4PHGjx2Kux1tRRs13nLdKo5648/Skve7cWzEeb0tWCExjCd9eVzCrYmZSOlvcZ16W7I6eNMM1xtvAju1W9Rbe1ubRnvM28J6JmTaudcbe2/O1vhX82hV2q1xp97WbgXN/YsN7S135kvKW2puSZQrR1PDKFbewsZ39BgEe7Hh8xY5ZkK7ot4pTKfespZhwfUWub1DemP9d5PY5RyG6lrWo3SqGpzPW+J8nYVdOPY9XK7UmzU+KGpvzSzuiLfAHjjNR28/ZWPAqlEv9mgs7Vq7SyxX2k8z6+kmEcai5hl4ueV0YJiqfhodPPkmcgj2euNpZTCZ6e+DF05DBSuaOzW+0nEhdvNj7q0aNrtv+7gwDpqzC/ZFxPzd603MmhlxmuhLNbhwezs/D9m6WZPwtrEGhd4xb6MDb6ypv+Pvfm+9cCcNic48PvTmZnCd572xGz6/vUVVoAZGXe689lYcbW+8bmmtbi4vFSYWbo27n2f5ltccbwtrwiOpR4RBo8m2e6sOHqV72dlavTEi0V1D9+Hoodv1kJPzeuWtbBtPxdgyssu15L0NQeJj6YSDw8wslA3uYDD28Nze7n9Q/OG73NA/ojreKueWdAZit9kj3vLGiMy7rcg0bG+ei+T12LlrmUcbOl7vPbVuqfO2WD7HDcqbmDpa5Vq8jRvf0E5ZXAXBWgXfud6KujvPTnbUS6yT25VNebfweAvtAUA8mHXGyzdkQnVsfspUrdQDvdQSMzN/W+iqmFOohHcTWBl65VHY+e8yYllrV68PhtyiWn8Ue808ged6K7HiWC2FGzNTSPXj55g33i6HQktSmMGEp0ITfppxHBSPtbcq2NcnSfbqGqJfLOvNzJO+XeB3wJnMiTbyPfR7U7noRuVP1gxroXQdXUeqRPl9vlXphSDWZ33k29yb+Iltl+di16xRS1nJa/DWS/RKKr+fRN2/2Gev945W6qDVqOmNL2+IbdfbzjpBkjVOIIOxjO2FZL6jtOqjpzPhVsd2njn1Zf4fMprv4zjez1Utq7Rfj3Kh2hCM55M4nsxrM1HaT/WvAWx7WpdLm6PMoHGCKM3NCRTTLI534g8Wo1SWHi3zHTtw3ZgEhktWOM5L70rEdfyv5v8HvNGANxrwRgPeaMAbDXijAW804I0GvNGANxrwRgPeaMAbDXijAW80Hn5WwNtT+PP9LzXv/9IxeDvNm9df17z9Scfg7TTwRgPeaMAbDXijAW804I0GvNF48/Z1je3tTuqwfDzomOWtjt3dore/P/xY8+EfHfv4reLTk2MAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACSfwE7SpSo7gwpagAAAABJRU5ErkJggg==' }},
            { title: 'coffee house',type: 'image',tags: [ 'default', 'coffee', 'beans', 'latte', 'cafe', '3:00 pm break' ],contents: { message: '<div>habit?</div><br/><div>* refreshing</div><div>&nbsp;&nbsp;* drink</div>', iSrc: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQsAAAC9CAMAAACTb6i8AAABKVBMVEX////9xhP9/f2AUyr8///9wwD6wBTxshX8wxT5///2uBX9/fzsrBb5vBT9xQD9wgDopRburxThnRT9/fj/wQD9/fX5xhN+VCqBUyjopxXWjhbblhXfmxX93ZD5yR36+vP734z32Gn42XL08e7789P89uD75aj94pj924v+0Fb389T74p747cb68MH9+Or71Gf612D757n5zkj56bP78eD72Hr5ySp7Shr+35x3RQv4zSz44I/OhRb80mL36q/3zkT8yTn10UjSwbHCrqGulICdfmaRb1CHZD6ZeFumjHG3ppfa1Mrj39zSwbtvNAB1RxByThvjrCfw2afzx4T1xUTux3jfxKXGfi3ruFbCZgDCcwvZq3nr28zuzpLpqT3Un2Tiw57KdwDXmE7ZXN/wAAAP0ElEQVR42u1dW0MbObKWqkvQooxEkFvdzgSb+Bpj01wc7HGyCR5yIQnscmb3TM5Odmd35vz/H3EefIXFbjUPZ9pMf08QsEN/rstXpZLEWI4cOXLkyJEjR44cOXLkyJEjR44cOXLkyJEjR47/RxgyhgwAjL/4Y5OBi/ijPTwxBDSMISILGvWTZmv/xYsXL17st5q9eiNgiMiYYYCMHj8ZYAJEaPT22mWltFBzCK1Uub3XawBiYOAPYBfSHHSOrdbK+tznPp/D5z73rdLaHncOjHzEdgGAwKTpVq3WilvOfc655Xfhc2650tpWu0YyQGSPz0AMIuJh1WrN3aC1rR4iAgSPMGPEnb4Qvr3lFqtgfSHKrfixRUuDslEViqeHUtWGRPNoIqlBrB8pwR8GLY7qiOZxGAXDxrFQzq7xH8HUKnHcQPYITMPIYF+t8g7d9xNpUmI/kOtvGvKkrK1d/piiX+nYJDIs1+UTuc6qCpAwfqN8u+ohVV0+ty6uot7EjCSsqQIzJHt8tZywxRYGfbdYIuyJNOvqKDJ4rpM+8lfGNBzVl8/5UbCWtawxeNBPzKOih7Lprjt0/wDX0DQMe2sTP3DbN4BVdy6stm/ZunFBiPsOj6ib0uC5Oxc+52Ifca3iJ8jguYvOVDFB0E+nvcTzYL3K+eBYl5MzpTqSRHG6MsWW9fEaeQmx4NQpN4gmArqmkYWXtYN1aQCSDE6Fk2LQDQBIz4UvTtfFTTD4QWxYp6cyD+LCbogf1kJoBGSOi0lNmglTIwSgWNz9qYMMLR6bNeh3kawWy0lV+OSLUwSAu7FTq/vaoHdQLlYz7yXEcF+sfhTLba8858KY2zlVHx+oZLuwXO/LbAdQMvJtkvv7ukmTL9sIQHCqbgnLuO4WQPRbabJMBph6UgPLLx7J7iREnDIAwtYtLpr4WrhJUFXPcquLKOgnKSerGrI1+eT7CEBQL857OeIcse0ovlQ/IMpusGBHyZXpGcj25LdEAMAY9P25cuihuyYXRyzDIaOX2K/gooHTaOnrBoBhC1WcLRuoOOsNq3vZLdNjB8M+RapMEoWvmmAAaJ44xD6jbhrtFWe0NCF5nOzpqoXQmDqSOpYVBMDZ68QJ4GEKLtRxJlUGETYdqhDRDeBg6kk+rwAAmO6Mixiwp9NUJs0sanEDcTmxt+9bES9WILorAcjgaJKIfQN4mGKBzbflOIOdcYMvtEMhoQwsdCx0FQHIwKGwnHOuRtLMPchJZOgXGVxDgoayLh9khYBmlFkbIwARjsZEnUsDcaqFV6sa2RNc8kg5fZAVADlXU7qDAAB4Mg4hIwTEUapGlz3KXsSoCxez4KICIDti1onoAwAA4Pn4nwJjYD8dF7qeuZrsyO0JRB3IHIiFBRIwAGRONPc5FzEBHmibZk1eHWWrRoPgpXL7+1XTQADzHqA6BwIAQyOfc657QIDnqcYTfPUyyFLIIFkVboui6gjMLSUiTsbRs6Mt52IfDWBT+WlSiXieqVSC7o39spEAZmHtvS0BAKDCLee8TwRQKaecY4qzFD6x5SwWRRcBsDP7fSu64+j5XHHORRcWY6vjKmsrQ1wQuq99+VUEMJXydBjHV0dIBAA9zTlXZ0gEgbWpyOhnR3sa7GqbwqIBCDpTkepzFQMA0FhvqtgYkq2UabWbmdk2gmqKekrvIZCpzOc8dVMSgJGj8U8lmcCkswtdzYphgDHpYl1ABNiZpRLRlgRAk8q9bwxQivgzWXTKSOuTUpWWnIuOJIDK/DU2BgCDZ2r8UwSgRroFZ32YlT5GmmkSzjkvVxAWR1BEnQAMjudzRNkAgHyT6i1VNSMbcUim0wO2+BYAaN6zUT0EoOmskuhhANhMl1bLWbGLg5TO7d9ZO9QtADBTLnQbEfBlSolxkJE88r1IFfZ9y18i0ThvcM65PkMAwNkMW4xgAptmaNqK7zPCxRuVLgVy3ZPGzLsYqooANKvVxQkCoeMIx7Sj8yYj8sJPN/buW1FFIHk26/hWEYDYtBuu9tEAttMUaNz3sxAviB3YtFsA1CkanCcSvc8ASE77WeoNSmRtlY7fgwwsoRGeFFNvhxgZIjbjQnQAFjrC6hQJsJ0qePrFkwxMOhq5l36/UN+QweOiEErZfn90ggDQ00oIIZRSp0gYjNIlErWXgSaGSSmLOLe+6hsy0Goe1uM4DqQBADAmjhvdXqd6yn/AgGKVLgqpNxngAjDlrGpRjKpNMGY8fQZQq9WAgJnKZH8ImtiQ6eqUoqWPv39FQkGaCWatTpsNZGgM1C7evf/w8dKLhh8YAnsXXX789PnLn69qhhFB0DuzWvMUIiP4/eMFuJdRvho1Y4ZMXl0zYO+Hg0EURZE3uGBAdDkIo2gwuBkOoyswyBiaw2PhbhxZWDNCZwUuRicBstqfP4R/uWKs5oXeGIMaEnt3E3qeF4ZRGHo1Ylcfv1wzho3nWjmr8N+/OnNcF/eVagLS1efBMAqjimHvBpHneZ4XDj4wwNqUGM8bfGJInwaD4cd3NSbr5476U/cywEXTgQu/rEcNZBeXN5HnedFnRrUwmjz7zQVj7MNgzsUFkxfD0POim/BLDYOqW3LVWZg+6LjYhWhX8PrTTeSFnucNrxj7PJhYQnRpkL0bzqjwohrVwiiMPM+LBtEFc+yK604GRHjLyS4q7Oommj49sYub6aPfvGPsam4V3uA9M3MriYbvGTp1U3UrA1zsOXxsqimvhwtOcTWYfhOFFXb1X9ECFzX2fjiNHmHoDd+z2KXgEXtrwoWI2cdo5hTsajBPIu/YxSCaR87oM3u/YCWeFw6v2XP1iLiwAUWzB778MpjbQfTp/dC79eyXt6jwvOEFOxOPigv2afb8UbRgB96tbzzPC+98791cs+qa2AU6lakVdj0MPe/OYyYi8sLhe4Yu1Z/ay4C++N7lD60AXg0GaanwvGj4nghPXf6L739/LpzSv6oQsOtP4/wQuTMxCC8YQTByW4FaDw0uKgaA0cXH4cAL3awjDKOb8EuFAUBlXTQ4c5nf1g0kIEK4+hwOB5GTSdx8fFdBNAAQO2mtbgZip8tsqugiAQAQY7WLz+FwEIVeOK5IQs+LoiiKFuxhMBhGn95dM4ZECAD1osNiiWhkIF64zKbqE6Dx8CIwZOb64vNlNBwOBlEURp4XhmEYhlEURoObm+HN5acvVzXG2OQFgI03vnaQcxmIF+ggkIsdmMEAECKrXY/7WpdhNLgZDAZReHn56fOXi6uaYYjGEMzIMLJycp64jS0Dq8vEHOZ0xZmccUEAQATTv5xqE8yGNAGIYBEEhNi03F/RDlajTKyPOEy5qjbCMsy7hSshG3bVtj51lIH1EXAaruobeCBoEmlI1leNkKqWzEAfHA4dxlx1/FAujDFmTCS27IqB18MMjGwRi1Vyf2EyxPkgMhrdSfyo+MuXU1WcgXhhGHPYT6Sb+FAqZLN4XkcAgPnK/D37i1g2ztGpJs+iiKOHc3EmhH0ZABAsHVyyosqyAZeKZPRQLgDPfa5fSADAulhejWSEC5eVMx3Dw1KJqSjOxTECgFk+xCUa2aCCMPlMMa5PDD3MLrqKc1FFAlhe+oh+Rs6ZIpcJDFHFh9kFtgTnqiMJgJbO1GahqTVRGC4nVvTlA+2izW3xvBIAAC5tG+l6RgbC0cArh7L9pXwQFRUlxFlgDADI82VcvAKTDcNAwpZDVu08LJN0+VkdpUGipTHaihZSZrbTOJwLVWw/jIu4MrYnQ9TWS9tmLENoW4dmywNrs0nMlXvL9mradpaooF5iVrWiSQ9Jq+M6lXBVOdzL1N47h41A+hSDB2tPGa8Yw+ibLHFhZCvJMHyuDtKbxbhCxaBpl7+/aGXqQE8Ch7l+sZ9aYhAiYqV7tvKEWBtk6ggMIPnaYStiLXUqqff22lYXV7118bWkLO3hvnXAx6qlLUpjGYYam0KLhLMKVUyQseNAIHldVZ1juliBv+xu+gmz0KKTvbNAwCTPQusupuKi8uNOaTMhDvWDLJ6Xk6wx1CmZNFz8dadUKG2uHIQWvQxexwHGYVtUuhaw+bFUKhUKm6smaE8xgwcUImAjMZWIfpwin/6tVCoVCk8KWyt2sh8gy+Rpt7Kl/QQ2ij9UZMVlUQSM/KVUKBUKT548ebLF7w2g1s/UaQ+3vCTAc53gJeXif9ec+hhofioVSoVC4cmTzc3N7XtDhq/Ps3p3jzFwYJO2C/p86ysQmski8z2uYQgQZPfH0s6Miq3N7ftd5ACye9MCugyHb/7PV0JcVrMSGRl0/75TKBXmVGxubdxjGKqT5YOxKXmbv+/zrZ+//eOfRuI9S+uMoXn5y792S6U5FZubW1tb2xv3bejP8nm/xIKRwybmzW8///zt37/+s2LGSQCRoUTGyLz860//2t3ZGVPxZIGK7buWYdUoQMg0F6zhsN9YbX779u2333777U//++9//Prr169fv/76y99++vuz3d3d3Z2dnZ27VrG1vb19xzJ81WDZvryICLvJLS7ON799+9MET58+ffr06XffPXv2bHd3d2fMRaFQKBTmVIy58O+Ow2X9tHTjdufO5i0qvvtuysXuzs5OYe4gW5sTIrY3NjYWyVDNNbjvDAKnZMK37qNiwSruOMiYC3++iwiD7N+DR2jwtTMZMyq+e/ZsGiruUjHjwp/V7/o10nrc1GOw5bB5UG3dsordiYOUFnXFolGMufA5575qrc1dkcZgy+HECr512yp2d+5zkEUPmXBhW+t0sRfha4chLr51N1b8p67YmlqFPwUXLVynKzPRyJ5LNtl6+vRWBiktDZszLrh+K8063X5HEGDdJspxzrfnwWKprthepELwrjTrdkEkYuNUJ7vJ9kLYXKorpkxwX4wOYP3uACQwdKYTBLnv8+07VrHCQbgoVgnW8H5dZECsl3Qlos/59jRUrNQVvs+5Vj1GDNf1jvL4WCRdxKS2nXRF2RenL5GtLwzIpi0mucmGi64o2g6u95XLRDJ+LhRfLTa2p/2821QsiE0h3sTIaJ3tgjEGEusJCcXn26XSirDJefG8jobYuiMgInn4qqhWkrGxTFds+L6v+z1EoPW/n32cVOjkXFu7Yv/HxpPCEl2hR71194071ZrB7qledVjdxthD7kRNrU8PgYx5TFwQEsN4vy+WXHbo+3xj6h9jLvwNXxfVWQMZIXtMXEyKV2lOqlbfP5roc39z5iDbG7ZY9I96FZTscQLIIJruWZmLeyPpxswotrft2WEgjaGAPV4QY4w1emevrC6qcbXiW8659bnl/va44X3UrDOGDBgie+QgBgiVenPv6JUVSmihtRZaKMH77RedeizRMGJ/DBAZJENGImIQN+pjNGKDEhENGiT6o3CRI0eOHDly5MiRI0eOHDly5MiRI0eOHDly5MiRI0fW8H8pi6WsIBpvugAAAABJRU5ErkJggg==' }},
            
         ],
         // the note being selected for operation. There might be chances that a selected note would need to be 
         // brought forward to another page / view and there is no way to directly forward this info around; 
         // hence the store would be the right place keeping this instance alive for some moment.
         pickedNote: {},

      };
   },
   mutations: {
      // addNote - push the given [note] object to the noteList
      addNote(state, note) {
        state.notes.push(note);
      },
      // setPickedNote - this note would be the one moving around within the pages / views of the app.
      setPickedNote(state, note) {
         state.pickedNote = note;
      },
      // updatePickedNote - merge changes to the picked-cloned note, then update the notes repo based on the merged note.
      updatePickedNote(state, note) {
         // merge
         let _n = state.pickedNote;
         _n.tags = note.tags;
         _n.contents = note.contents;
         // update back the notes repo
         // [logic] title + type match (non a very good solution... though)
         const _ns = state.notes;
         for (let _i=0; _i<_ns.length; _i++) {
            const _n1 = _ns[_i];
            if (_n1.title == _n.title && _n1.type == _n.type) {
               state.notes[_i] = _n;
               break;
            }
         }
      },
      // removeNote - based on the [pickedNote]'s attributes, try to remove this note from the [notes] array.
      removeNote(state) {
         let _n = state.pickedNote;
         let _removed = false;
         // must have a valid "pickedNote" or else...no way to remove it~!
         if (_n) {
            for (let _i=state.notes.length-1; _i>=0; _i--) {
               const _n1 = state.notes[_i];
               if (_n1.title == _n.title && _n1.type == _n.type) {
                  // object / array level match
                  let _same = true;
                  const _n1Tags = _n1.tags;
                  const _nTags = _n.tags;
                  // length
                  if (_n1Tags.length != _nTags.length) {
                     _same = false;
                  }
                  // tags
                  if (true == _same) {
                     for (let _i2=0; _i2<_n1Tags.length; _i2++) {
                        if (_n1Tags[_i2] != _nTags[_i2]) {
                           _same = false;
                           break;
                        }
                     } // end - for(loop of tags)
                  }
                  // [debug]
                  //console.log(`tag same? ${_same}, ${_n1.tags} vs ${_n.tags}`);

                  // contents?
                  if (_n.type == 'plain' && _n.contents != _n1.contents) {
                     _same = false;
                  } else if (_n.type == 'list') {
                     // same length?
                     if (_n.contents.length != _n1.contents.length) {
                        _same = false;
                     }
                     // same content?
                     if (_same) {
                        for (let _i3 = 0; _i3<_n.contents.length; _i3++) {
                           const _nNote = _n.contents[_i3];
                           const _nNote1 = _n1.contents[_i3];
                           if (_nNote.message != _nNote1.message || _nNote.checked != _nNote1.checked) {
                              _same = false;
                              break;
                           }
                        }
                     } // end - same length (criteria 1 ok)
                  } else if (_n.type == 'image') {
                     if (_n.contents.image != _n1.contents.image ||
                        _n.contents.message != _n1.contents.message) {
                        _same = false;
                     }
                  }
                  // match?
                  if (true == _same) {
                     state.notes.splice(_i, 1);
                     _removed = true;
                  }
               } // end - if (title and type matches - top level check)
            } // end - for (loop of notes)
            if (!_removed) {
               console.log(`something's wrong -> no matched note? picked -> ${state.pickedNote}`);
            }
         }
      },

   },
   getters: {
      // noteCount - return the number of note(s) available
      noteCount(state) {
         //return Object.keys(state.notes).length;
         return state.notes.length;
      },
      // pickedNote - return the targeted / picked note. 
      // However, this instance should be READ-ONLY. If changes are applied to this instance, 
      // then the changes will be permanent. 
      // In case you need a temporary, modifiable instance, call the [clonedPickedNote()] instead.
      pickedNote(state) {
         return state.pickedNote;
      },
      // return a shallow clone of the picked note instead.
      // (if returning the actual instance... altering the contents would actually directly alter the object / proxy)
      clonedPickedNote(state) {
         let _n = state.pickedNote;
         // [deprecated] brute force approach
         /*
         let _clone = {
            title: _n.title,
            type: _n.type,
            tags: [].concat(_n.tags),
         }; 
         // revise logic on shallow clone
         switch (_n.type) {
         case 'plain':
            _clone['contents'] = _n.contents;
            break;
         case 'list':
            _clone['contents'] = [].concat(_n.contents);
            break;
         case 'image':
            let _c = {};
            _c['iSrc'] = _n.contents.iSrc;
            _c['message'] = _n.contents.message;
            _clone['contents'] = _c;
            break;
         }
         return _clone;*/
         if (_n) {
            return JSON.parse(JSON.stringify(_n));
         }
         return { 'message': '[error] non supported operation since pickedNote state is MISSING~', };
      },

   },
})

export default store;


