// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// notes remake vite + vue3
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { createApp } from 'vue'
import App from './App.vue'
import './index.css'

// setup store, router
import store from './Store'
import router from './Router'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'
/* import specific icons */
// bold, italic, underline, copy, paste, clear-all
import { faPhone, faUserSecret, faAngleLeft, faListUl, 
   faCircleQuestion, faCircleXmark, faEnvelope, faFolderPlus, 
   faPenToSquare, faNoteSticky, faCommentDots, faBookOpenReader,
   faPen, faTags, faCircleCheck, faEraser,
   faBold, faItalic, faUnderline, faCopy, faPaste, faTrash, faFloppyDisk,
   faImages} from '@fortawesome/free-solid-svg-icons'
import { faAddressBook, faCircle as regCircle, faCircleCheck as regCircleCheck } from '@fortawesome/free-regular-svg-icons'
import { faGithub } from '@fortawesome/free-brands-svg-icons'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* add icons to the library */
library.add(faEraser, faCircleCheck, regCircleCheck, regCircle, faFloppyDisk, faTrash, faBold, faItalic, faUnderline, faCopy, faPaste, faTags, faPen, faBookOpenReader, faNoteSticky, faCommentDots, faPenToSquare, faImages, faFolderPlus, faGithub, faEnvelope, faCircleXmark, faCircleQuestion, 
   faAngleLeft, faListUl, faPhone, faAddressBook, faUserSecret)

createApp(App).component("fa-icon", FontAwesomeIcon).use(store).use(router).mount('#app')

// free-brands-svg-icons